from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'landing.views.home', name='home'),
    url(r'^landing/',include('landing.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^logout/$','landing.views.logout',name="logout"),
)
