from django.contrib import admin
from landing.models import Vpsnode,Domain,GoogleCredential,NamecheapAccount,Ip_Address

class VpsnodeAdmin(admin.ModelAdmin):
	list_display = ('domain_name','reverse_domain','ip_address','host_url',
		'account_email','cpanel_url')

class DomainAdmin(admin.ModelAdmin):
	list_display = ('domain_name',)


class GoogleCredentialAdmin(admin.ModelAdmin):
	list_display = ('google_username','spreadsheetname')

class NamecheapAccountAdmin(admin.ModelAdmin):
	list_display = ('namecheap_username','namecheap_api_key',)

class Ip_AddressAdmin(admin.ModelAdmin):
	list_display = ('ip_address',)

admin.site.register(Ip_Address,Ip_AddressAdmin)
admin.site.register(NamecheapAccount,NamecheapAccountAdmin)
admin.site.register(Vpsnode,VpsnodeAdmin)
admin.site.register(Domain,DomainAdmin)
admin.site.register(GoogleCredential,GoogleCredentialAdmin)
