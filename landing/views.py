import requests
import gspread
import random
import os
import socket

from landing.scripts import Api,ENDPOINTS,NAMESPACE,gen_hosts,record,keys
from landing.forms import VpsnodeForm,DomainForm,DomainConfigForm
from landing.models import GoogleCredential,Domain,Vpsnode,NamecheapAccount,Ip_Address
from landing.models import Domain as DomainModel

from django.contrib.auth import authenticate,login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.forms.formsets import formset_factory, BaseFormSet
from django import forms

from xml.etree.ElementTree import fromstring
from datetime import datetime

def home(request):
    form = AuthenticationForm()
    home_dict = {}
    if request.method == 'POST':
        if 'login' in request.POST:
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(username=username, password=password)
            if user is not None and user.is_active:
                login(request, user)
                return HttpResponseRedirect('/')
        else:
            form = AuthenticationForm(form)
            c = RequestContext(request,{'form': form,})
            return render_to_response("home.html",c)
    if request.user.is_authenticated():
        try:
            NamecheapAccount.objects.all()[0].namecheap_username
        except:
            warning_message = "warning"
            home_dict.update({"warning":warning_message})
        return render_to_response("home.html",home_dict,context_instance = RequestContext(request))
    return render_to_response("home.html",{"form":form,},
        context_instance = RequestContext(request))


def config_domain_view(request):
    """This view shows input panel for domain config."""
    try:
        USER_NAME = NamecheapAccount.objects.all()[0].namecheap_username
        NAMECHEAP_API_KEY = NamecheapAccount.objects.all()[0].namecheap_api_key
    except:
        pass
    form = DomainConfigForm()
    if request.method == "POST":
        form = DomainConfigForm(request.POST)
        print "check before"
        print form
        data = requests.get('http://wtfismyip.com/json').json()
        my_ip = data['YourFuckingIPAddress']
        domain = request.POST.get("domain_name")
        subdomain = request.POST.get("subdomain_name")
        print subdomain
        frontend = request.POST.get("ip_address")
        ip_query_set = Ip_Address.objects.all()
        ip_list = []
        for ip_entry in ip_query_set:
            ip_list.append(ip_entry.ip_address)
        redirect = random.choice(ip_list)
        spf = 'v=spf1 a mx ip4:%s -all' % frontend
        mx = '%s.%s.' % (subdomain, domain)
        new = [
            record('*', 'A', redirect),
            record(subdomain, 'A', frontend),
            record('@', 'A', frontend),
            record('@', 'MX', mx),
            record('@', 'TXT', spf),
        ]
        api = Api(USER_NAME, NAMECHEAP_API_KEY, USER_NAME, my_ip, False, True)
        if form.is_valid():
            xml = api.domains_dns_setHosts(domain, new)
            domain_create = DomainModel.objects.create(domain_name=domain,
                            subdomain_name=subdomain, ip_address=frontend,
                            availability="nonavailable")
            domain_create.save()
            return view_dns_hosts(request,domain,subdomain,frontend)
        else:
            print "subdomain:" + str(subdomain)
            print "frontend:" + str(frontend)
            if subdomain != "" and frontend != "":
                print "yo"
                try:
                    domain_model = DomainModel.objects.get(domain_name=domain)
                    domain_model.subdomain = subdomain
                    domain_model.ip_address = frontend
                    domain_model.availability = "nonavailable"
                    domain_model.save()
                    xml = api.domains_dns_setHosts(domain, new)
                    return view_dns_hosts(request,domain,subdomain,frontend)
                except:
                    pass
            else:
                return render_to_response("landing/domain_config.html", {"form":form},
                    context_instance=RequestContext(request))
    #list domains from database
    domain_objects = DomainModel.objects.filter(availability="available")
    paginator = Paginator(domain_objects,7)
    print "hey"
    #PAGINATION CODE
    page = request.GET.get('page')
    try:
        domain_objects = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        domain_objects = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        domain_objects = paginator.page(paginator.num_pages)
    return render_to_response("landing/domain_config.html", {"form":form,"domains":domain_objects},
        context_instance=RequestContext(request))


def view_dns_hosts(request,domain="",subdomain="",ip=""):
    USER_NAME = NamecheapAccount.objects.all()[0].namecheap_username
    NAMECHEAP_API_KEY = NamecheapAccount.objects.all()[0].namecheap_api_key
    data = requests.get('http://wtfismyip.com/json').json()
    my_ip = data['YourFuckingIPAddress']
    if request.method == "GET":
        domain = request.GET.get("domain")
        subdomain = request.GET.get("subdomain")
        ip = request.GET.get("ip_address")
    api = Api(USER_NAME, NAMECHEAP_API_KEY, USER_NAME, my_ip, False, True)
    hosts = api.domains_dns_getHosts(domain)
    domain_id = DomainModel.objects.get(domain_name=domain).id
    return render_to_response("landing/view_hosts.html",
                    {"hosts":hosts,"ip":ip,"domain":domain,"subdomain":subdomain,"domain_id":domain_id},
                    context_instance=RequestContext(request))

def edit_domain(request):
    """Edit domain"""
    if request.method == "POST":
        domain = request.POST.get("domain_name")
        subdomain = request.POST.get("subdomain_name")
        ip_address = request.POST.get("ip_address")
        availability = request.POST.get("availability")
        domain_instance = DomainModel.objects.get(id=request.POST.get("id"))
        domain_instance.domain = domain
        domain_instance.subdomain = subdomain
        domain_instance.ip_address = ip_address
        domain_instance.availability = availability
        bl_dict = {"black_list":False}# check_blacklist(domain)
        if not bl_dict["black_list"] == True:
            domain_instance.save()
            return HttpResponseRedirect('/landing/view-domains/')
        else:
            DomainForm(instance=domain_instance)
            return render_to_response("landing/edit_domain.html",
                        {"form":form,"bl_dict":bl_dict},
                    context_instance=RequestContext(request))

    else:
        domain_id = request.GET.get("id")
        domain_obj = DomainModel.objects.get(id=domain_id)
        print domain_obj.availability
        print "asdasdas"
        form = DomainForm(initial={"domain_name":domain_obj.domain_name,
                                   "subdomain_name":domain_obj.subdomain_name,
                                   "ip_address":domain_obj.ip_address,
                                   "availability":domain_obj.availability})
        print form
        return render_to_response("landing/edit_domain.html",
                        {"form":form,"domain_entry":domain_obj},
                    context_instance=RequestContext(request))

def add_node(request):
    """
        This function is responsible for the add node view.
        The same view accepts post request from the page and adds
        nodes to the database.
    """
    if request.method == "POST": #if submit button is clicked, it enters here
        form = VpsnodeForm(request.POST)
        if form.is_valid():
            check_bl_result = check_blacklist(str(request.POST.get("domain_name")))
            check_reverse_result = check_reverse(str(request.POST.get("reverse_domain")),
                                   str(request.POST.get("ip_address")))
            print check_bl_result, check_reverse_result
            if (not check_bl_result["black_list"]) and check_reverse_result["match"]:
                vps_object = form.save() #save to database
                form_dict = {
                                "ip_address":str(request.POST.get("ip_address")),
                                "domain_name":str(request.POST.get("domain_name")),
                                "reverse_domain":str(request.POST.get("reverse_domain")),
                                "host_url":str(request.POST.get("host_url")),
                                "account_email":str(request.POST.get("account_email")),
                                "account_password":str(request.POST.get("account_password")),
                                "root_password":str(request.POST.get("root_password")),
                                "cpanel_url":str(request.POST.get("cpanel_url")),
                                "cpanel_username":str(request.POST.get("cpanel_username")),
                                "cpanel_password":str(request.POST.get("cpanel_password")),
                                "price":str(request.POST.get("price")),
                                "notes":str(request.POST.get("notes")),
                                "comment":str(request.POST.get("comment")),
                                "status":str(request.POST.get("status")),
                }
                form_values = [
                                str(form_dict["ip_address"]),
                                str(form_dict["domain_name"]),
                                str(form_dict["reverse_domain"]),
                                str(form_dict["host_url"]),
                                str(form_dict["account_email"]),
                                str(form_dict["account_password"]),
                                str(form_dict["root_password"]),
                                str(form_dict["cpanel_url"]),
                                str(form_dict["cpanel_username"]),
                                str(form_dict["cpanel_password"]),
                                str(form_dict["price"]),
                                str(form_dict["notes"]),
                                str(str(datetime.now().month)+"-"+str(datetime.now().day)+"-"+str(datetime.now().year)),
                            ]
                #notice = update_google_docs(form_values)
                c = RequestContext(request, {"form_dict":form_dict,"node_id":vps_object.id})#"notice":notice})
                return render_to_response("landing/view_node_detail.html",c)
            else:
                check_reverse_result["match"] = True
                c = RequestContext(request, {"form":form,"bl_result":check_bl_result,
                                   "rev_result":check_reverse_result})
                return render_to_response("landing/add_node.html",c)
        else:
            c = RequestContext(request, {"form":form})
            return render_to_response("landing/add_node.html",c)
    else:
        add_node_form = VpsnodeForm(initial={"status":"Unused"})
        c = RequestContext(request, {"form":add_node_form})
        return render_to_response("landing/add_node.html",c)

def view_domains(request):
    """
        This functions fetches all domains in database and paginates it.
    """
    domains = Domain.objects.all()
    #for tryer in xml2:
    #    print tryer.attrib
    #print xml2
    #for domain in domains:
    #    results.append(domain.attrib)
    paginator = Paginator(domains, 7)
    page = request.GET.get('page')
    print page
    try:
        domains2 = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        domains2 = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        domains2 = paginator.page(paginator.num_pages)
    return render_to_response("landing/view_domains.html", {"domains":domains2},
        context_instance=RequestContext(request))

def view_nodes(request):
    """This view displays all created vps nodes."""
    if request.method == "GET":
        if request.GET.get("order") == "asc":
            operand = ""
            setOrder = "desc"
        else:
            operand = "-"
            setOrder = "asc"
        if request.GET.get("sort") == "status":
            sstring = operand+"status"
            vpsnodes = Vpsnode.objects.all().order_by(sstring)
        elif request.GET.get("sort") == "date":
            sstring = operand+"date_added"
            vpsnodes = Vpsnode.objects.all().order_by(sstring)
        elif request.GET.get("sort") == "domain":
            sstring = operand+"domain_name"
            vpsnodes = Vpsnode.objects.all().order_by(sstring)
        elif request.GET.get("sort") == "email":
            sstring = operand+"account_email"
            vpsnodes = Vpsnode.objects.all().order_by("account_email")
        else:
            vpsnodes = Vpsnode.objects.all()
    else:
        vpsnodes = Vpsnode.objects.all()
        setOrder = ""
    return render_to_response("landing/view_nodes.html",
                              {"vpsnodes":vpsnodes,"order":setOrder},
                              context_instance=RequestContext(request))

def check_reverse(reverse_domain,ip_address):
    """Checks if reverse domain and ip address inputted matches.
        using sockets module.
    """
    message = {}
    try:
        name, alias, addresslist = socket.gethostbyaddr(ip_address)
        if name == reverse_domain:
            message.update({"gethosterror":False,"match":True,"check_message":"Reverse dns and IP reverse matches."})
            return message
        else:
            message.update({"gethosterror":False,"match":False,"check_message":"Reverse dns and IP reverse didn't match."})
            return message
    except socket.herror as e: #address related error
        message.update({"gethosterror":True,"error_message":"IP input error: "+ e.strerror,"match":False,
                        "check_message":"Error in input"})
        return message
    

def add_domain(request):
    if request.method == "POST":
        form = DomainForm(request.POST)
        if form.is_valid():
            check_bl_dict = check_blacklist(request.POST.get("domain_name"))
            print check_bl_dict
            if not check_bl_dict["black_list"]:
                form.save()
                return HttpResponseRedirect('/landing/config-domain/')
            else:
                return render_to_response("landing/add_domain.html", {"form":form,"bl_dict":check_bl_dict},
            context_instance=RequestContext(request))
        else:
            return render_to_response("landing/add_domain.html", {"form":form},
            context_instance=RequestContext(request))
    else:
        form = DomainForm()
        return render_to_response("landing/add_domain.html", {"form":form},
            context_instance=RequestContext(request))

def check_blacklist(domain):
    """check spamhaus, uribl and surbl"""
    black_list = False
    host_str = "host %s" % domain
    ##check for blacklist using spamhaus dbl through host command
    command1 = host_str + ".dbl.spamhaus.org"
    print command1
    spamhaus_result = os.system(command1)
    print spamhaus_result
    if spamhaus_result == 0: 
        #this means it is black_listed.. it returns zero
        black_list = True
        return {"black_list":black_list,"result":"The domain "+ domain +" exists in SPAMHAUS blacklist."}
    ##proceed if not listed and check with urilb
    ##documentation here: http://www.uribl.com/about.shtml#implementation
    '''uribl_result = os.system(host_str + ".multi.uribl.com")
    print uribl_result
    if uribl_result == 0:
        black_list = True
        return {"black_list":black_list,"result":"The domain name exists in URIBL blacklist."}
    ##proceed if not listed in urilb and go for surbl
    ##proof that this works is this link http://www.surbl.org/guidelines
    ##we will use host command'''
    surbl_result = os.system(host_str + ".multi.surbl.org")
    print surbl_result
    if surbl_result == 0:
        black_list = True
        return {"black_list":black_list,"result":"The domain "+ domain +" exists in SURBL blacklist."}
    ##if not found in any of the 3
    return {"black_list":black_list,"result":"Not found in spamhaus, uribl and surbl"}

def update_google_docs(list_entry):
    """This function updates google spreadsheet during 
        add vps node Command"""
    counter = 0
    creds = GoogleCredential.objects.all()[0]
    messages = {"error_message":None, "success_message":None}
    try:
        gc = gspread.login(creds.google_username,creds.google_pw)
    except:
        messages["error_message"] = "Error in your google credentials."
        return messages
    try:
        spreadsheet = gc.open(creds.spreadsheetname)
        worksheet = spreadsheet.sheet1 #we will be using sheet1 to save data
        cell = worksheet.find("*endofline*") #find endofline character to easily find eol and insert from there
        label_start = "A%s" % str(cell.row) #start at A
        label_end = chr(cell.col+76) + str(cell.row)+"" #get end label
        next_line_cell = "A%s" % str(cell.row+1)
        cells = worksheet.range(label_start+":"+label_end)
        for cell in cells:
            cell.value = list_entry[counter]
            counter += 1
        worksheet.update_cells(cells)
        worksheet.update_acell(next_line_cell,"*endofline*")
        messages["success_message"] = "Successfully inserted entry."
        return messages
    except:
        messages["error_message"] = "Something went wrong in your entry in spreadsheet."
        return messages

def view_node(request,node_id=1):
    vpsnode = Vpsnode.objects.get(id=node_id)
    return render_to_response("landing/view_node_detail.html", {"form_dict":vpsnode,"node_id":vpsnode.id},
        context_instance=RequestContext(request))

class BaseDomainFormSet(BaseFormSet):
    """A custom formset for handling bulks of domains"""

    def clean(self):
        """Checks for blacklisted domains."""
        domain_list = []
        for form in self.forms:
            try:
                print "in"
                domain = form.cleaned_data["domain_name"]
            except:
                raise forms.ValidationError("Domain already exists.")
            bl_dict = check_blacklist(domain)
            if bl_dict["black_list"]:
                raise forms.ValidationError(bl_dict["result"])
            if domain in domain_list:
                raise form.ValidationError("Domains should have disctinct titles.")
            domain_list.append(domain)

    def save(self):
        for values in self.cleaned_data:
            obj = DomainModel()
            obj.domain_name = values["domain_name"]
            obj.availability = values["availability"]
            obj.save()

def add_domain_bulk(request):
    DomainFormSet = formset_factory(DomainForm,formset=BaseDomainFormSet)
    if request.method == "POST":
        print request.POST
        formset = DomainFormSet(request.POST)
        if formset.is_valid():
            formset.save()
            #return render_to_response("landing/add_domain_set.html",{"formset":formset},context_instance=RequestContext(request))
            return HttpResponseRedirect("/landing/view-domains/")
        else:
            return render_to_response("landing/add_domain_set.html",{"formset":formset},context_instance=RequestContext(request))
    else:
        try:
            extra_fields = int(request.GET.get("extra"))
        except:
            extra_fields = 2
        if extra_fields != None:
            DomainFormSet = formset_factory(DomainForm,extra=extra_fields,formset=BaseDomainFormSet)
        formset = DomainFormSet()
    return render_to_response("landing/add_domain_set.html",{"formset":formset},context_instance=RequestContext(request))

def vpsnode_update_view(request):
    """
        Returns a view for editing vpsnodes and retrieves post request
        to edit vpsnode.
    """
    if request.method == "POST":
        check_bl_result = check_blacklist(str(request.POST.get("domain_name")))
        check_reverse_result = check_reverse(str(request.POST.get("reverse_domain")),
                                   str(request.POST.get("ip_address")))
        if (not check_bl_result["black_list"]) and check_reverse_result["match"]:
            node_instance = Vpsnode.objects.get(id = request.POST.get("vpsnode_id"))
            node_instance.ip_address = request.POST.get("ip_address")
            node_instance.domain_name = request.POST.get("domain_name")
            node_instance.reverse_domain = request.POST.get("reverse_domain")
            node_instance.host_url = request.POST.get("host_url")
            node_instance.account_email = request.POST.get("account_email")
            node_instance.account_password = request.POST.get("account_password")
            node_instance.root_password = request.POST.get("root_password")
            node_instance.cpanel_url = request.POST.get("cpanel_url")
            node_instance.cpanel_username = request.POST.get("cpanel_username")
            node_instance.cpanel_password = request.POST.get("cpanel_password")
            node_instance.price = request.POST.get("price")
            node_instance.notes = request.POST.get("notes")
            node_instance.comment = request.POST.get("comment")
            node_instance.status = request.POST.get("status")
            node_instance.save()
            c = RequestContext(request, {"form_dict":node_instance,"node_id":node_instance.id})
            return render_to_response("landing/view_node_detail.html",c)
        else:
            check_reverse_result["match"] = True
            c = RequestContext(request, {"form":form,"bl_result":check_bl_result,
                                   "rev_result":check_reverse_result,})
            return render_to_response("landing/view_node_detail.html",c)
    vpsnode_id = request.GET.get("id")
    vpsnode = Vpsnode.objects.get(id=vpsnode_id)
    form = VpsnodeForm(instance=vpsnode)

    return render_to_response("landing/vpsnode_update.html", {"form":form,"node_id":vpsnode.id},
        context_instance=RequestContext(request))

def logout(request):
    auth_logout(request)
    return HttpResponseRedirect('/')