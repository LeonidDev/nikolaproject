from django.db import models
from django.contrib.auth.models import User
STATUS = (
            ('available', 'available'),
            ('notavailable', 'notavailable'),
    )
NODE_STATUS = (
            ('Used', 'used'),
            ('Unused', 'unused'),
            ('Failed', 'failed'),
    )
RECORD_TYPES = (
        ('A','A (Address)'),
        ('TXT','TXT Record'),
    )
class Vpsnode(models.Model):
    ip_address = models.IPAddressField(max_length=254,)
    domain_name = models.CharField(max_length=255,unique=True)
    reverse_domain = models.CharField(max_length=200,)
    host_url = models.URLField(max_length=250,blank=True,)
    account_email = models.EmailField(max_length=100,blank=True,)
    account_password = models.CharField(max_length=50,blank=True,)
    root_password = models.CharField(max_length=50,blank=True,)
    cpanel_url = models.URLField(max_length=250,blank=True,)
    cpanel_username = models.CharField(max_length=50,blank=True,)
    cpanel_password = models.CharField(max_length=50,blank=True,)
    price = models.CharField(blank=True,max_length=50)
    notes = models.TextField(max_length=1000,blank=True,)
    status = models.CharField(max_length=50,choices=NODE_STATUS,
                              default="unused")
    comment = models.TextField(max_length=1000,blank=True,null=True)
    date_added = models.DateField(max_length=255,blank=True,
                                  auto_now_add=True)

    def __init__(self, *args, **kwargs):
        super(Vpsnode, self).__init__(*args, **kwargs)
        self.args = args

    def __unicode__(self):
        return self.reverse_domain

    class Meta():
        ordering = ('reverse_domain',)
        verbose_name = "Vpsnode"
        verbose_name_plural = "Vpsnodes"

class NamecheapAccount(models.Model):
    namecheap_api_key = models.CharField(max_length="255",)
    namecheap_username = models.CharField(max_length="100",)

class Ip_Address(models.Model):
    ip_address = models.IPAddressField(max_length=255)

    def __init__(self, *args, **kwargs):
        super(Ip_Address, self).__init__(*args, **kwargs)
        self.args = args

    def __unicode__(self):
        return self.ip_address

    class Meta():
        verbose_name = "IP Adress"
        verbose_name_plural = "IP Adresses"

class Domain(models.Model):
    domain_name = models.CharField(max_length=50, unique=True)
    subdomain_name = models.CharField(max_length=50,blank=True,null=True)
    ip_address = models.IPAddressField(max_length=255,blank=True,null=True)
    date_added = models.DateField(auto_now_add=True,blank=True)
    availability = models.CharField(max_length=50,choices=STATUS,
                                    blank=True,default="available")

    def __init__(self, *args, **kwargs):
        super(Domain, self).__init__(*args, **kwargs)
        self.args = args

    def __unicode__(self):
        return self.domain_name

    class Meta():
        ordering = ('-date_added','-availability',)
        verbose_name = "domain"
        verbose_name_plural = "domains"

class DomainConfiguration(models.Model):
    domain = models.OneToOneField(Domain)
    last_updated = models.DateField(auto_now=True)

class Host(models.Model):
    """This is a host setting for a domain configuration."""
    domain_conf = models.ForeignKey(DomainConfiguration)
    host_name = models.CharField(max_length=250)
    ip_address = models.CharField(max_length=255)
    record_type = models.CharField(max_length=255)
    ttl = models.PositiveIntegerField(max_length=255,default=1800)

class Subdomain(models.Model):
    """This is one subdomain setting for a domain configuration."""
    domain_conf = models.ForeignKey(DomainConfiguration)
    host_name = models.CharField(max_length=250)
    ip_address = models.CharField(max_length=255)
    record_type = models.CharField(max_length=255)
    ttl = models.PositiveIntegerField(max_length=255,default=1800)

class GoogleCredential(models.Model):
    google_username = models.CharField(max_length=250)
    google_pw = models.CharField(max_length=250)
    spreadsheetname = models.CharField(max_length=250)