from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
	url(r'^view-domains/$','landing.views.view_domains'),
	url(r'^add-node/$','landing.views.add_node',name="add-node"),
	url(r'^update-vpsnode/$','landing.views.vpsnode_update_view',name="update-vps-node"),
	url(r'^add-domain/$','landing.views.add_domain',name="add-domain"),
	url(r'^config-domain/$','landing.views.config_domain_view',name="config-domain"),
	url(r'^view-logs/$','landing.views.view_nodes',name="view_nodes"),
	url(r'^view-dns-hosts/$','landing.views.view_dns_hosts',name="view_dns_hosts"),
	url(r'^view_node/(?P<node_id>\d+)/$','landing.views.view_node'),
	url(r'^edit-domain/$','landing.views.edit_domain',name="edit-domain"),
	url(r'^add-domain-bulk/$','landing.views.add_domain_bulk',name="add-domain-bulk"),
)