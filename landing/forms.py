from django import forms
from django.forms import ModelForm

from landing.models import Vpsnode, Domain


NODE_STATUS = (
            ('used', 'used'),
            ('unused', 'unused'),
            ('failed', 'failed'),
    )

class VpsnodeForm(ModelForm):
    class Meta:
        account_password = forms.CharField(widget=forms.PasswordInput)
        model = Vpsnode
        fields = ['ip_address','domain_name','reverse_domain','host_url',
                  'account_email','account_password','root_password',
                  'cpanel_url','cpanel_username','cpanel_password','price','notes','comment','status']


class DomainForm(ModelForm):
    class Meta:
        model = Domain
        fields = ['domain_name','availability']

class DomainConfigForm(ModelForm):
    class Meta:
        model = Domain
        fields = ['domain_name','subdomain_name','ip_address','availability',]

    def __init__(self, *args, **kwargs):
      super(DomainConfigForm, self).__init__(*args, **kwargs)
      self.fields["subdomain_name"].required = True
      self.fields["ip_address"].required = True
